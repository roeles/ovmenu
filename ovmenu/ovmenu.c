#include <curses.h>
#include <menu.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define MENU_W  50
#define MENU_H  10
#define TIMEOUT 20

char xcsoar_resolution[40];
char xcsoar_cmd[255]="/opt/XCSoar/bin/xcsoar -fly -";
char sensorcal_cmd[255]="/opt/bin/sensorcal -c";
char update_maps_usb_cmd[]="/usr/bin/update-maps.sh";
char update_system_usb_cmd[]="/usr/bin/update-system.sh";
char download_igc_files_cmd[]="/usr/bin/download-igc.sh";
char lan_ip[50]="";
char wlan_ip[50]="";

char *choices[] = {
                        "XCSoar", "Calibrate", "Update Maps USB", "Update System", "Download IGC", "Shell", "LXDE", "Power Off",
                        (char *)NULL,
                  };

void selectItem(int index);
void printTitle(WINDOW *win, int starty, int startx, int width, char *string, chtype color);
void refreshMenu(WINDOW* menu_win);


int main(int argc, char **argv)
{	ITEM **ov_items;
	int start_xcsoar;
	int c;
	int menu_delay=0;
	int ch;
	int fd;
	struct ifreq ifr;

	MENU *ov_menu;
        WINDOW *ov_menu_win;
        int n_choices, i, pos;

	printf("getopt ...\n");
    	// get commandline options
	// check commandline arguments
	while ((c = getopt (argc, argv, "r:")) != -1)
	{
		switch (c) {
			case 'r':
				// use config file
				if (optarg == NULL)
				{
					printf("Missing option for -r\n");
					printf("Exiting ...\n");
					exit(EXIT_FAILURE);
				}
				else
				{
					strcpy(xcsoar_resolution, optarg);
					strcat(xcsoar_cmd, xcsoar_resolution);
					printf("Using Resolution: %s\n", xcsoar_resolution);
					printf("XCSoar CMD: '%s'\n", xcsoar_cmd);
				}
				break;
		}
	}
	
	/* Initialize curses */
	initscr();
	start_color();
        curs_set(0);
        cbreak();
        noecho();
        timeout(TIMEOUT);
	keypad(stdscr, TRUE);
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_CYAN, COLOR_BLACK);
 
	attron(COLOR_PAIR(2));
	mvprintw(LINES - 2, 0, "Press F1 or Cursor keys to enter menu");
	mvprintw(LINES - 1, 0, "#########################");
	attroff(COLOR_PAIR(2));
        
	start_xcsoar=1;
        menu_delay=3000;
	pos=25;
        while (pos > 0)
	{
		ch=getch();
		if (ch != ERR)
		{
			switch (ch) {
			   case KEY_UP:
			   case KEY_DOWN:
			   case KEY_LEFT:
			   case KEY_RIGHT:
			   case KEY_F(1):
				start_xcsoar=0;
				pos=0;
				break;
			   default:
				break;
			}
		}
		usleep(25000);
	    	mvprintw(LINES - 1, pos, " ");
            	pos--;
	    	refresh();
        } 
        if (start_xcsoar){
			printf("%d\n",pos);
			system(xcsoar_cmd);
			clear();
        }

	/* Get IPs */
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
	ioctl(fd, SIOCGIFADDR, &ifr);
	sprintf(lan_ip, "%s", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
	close (fd);

        /* Create items */
        n_choices = ARRAY_SIZE(choices);
        ov_items = (ITEM **)calloc(n_choices+1, sizeof(ITEM *));
        for(i = 0; i < n_choices; ++i){
                ov_items[i] = new_item(choices[i], choices[i]);
		/* Set the user pointer */
		set_item_userptr(ov_items[i], selectItem);
        }
	ov_items[n_choices] = (ITEM *)NULL;
	
    /* Create menu */
	ov_menu = new_menu((ITEM **)ov_items);

	/* Set menu option not to show the description */
	menu_opts_off(ov_menu, O_SHOWDESC);

	/* Create the window to be associated with the menu */
    ov_menu_win = newwin(MENU_H, MENU_W, 4, 4);
    keypad(ov_menu_win, TRUE);
     
	/* Set main window and sub window */
    set_menu_win(ov_menu, ov_menu_win);
    set_menu_sub(ov_menu, derwin(ov_menu_win, 6, MENU_W-2,3, 1));
	set_menu_format(ov_menu, 5, 2);
	set_menu_mark(ov_menu, " * ");

    refreshMenu(ov_menu_win);

	/* Post the menu */
	post_menu(ov_menu);
	refresh();
	
    while((c = wgetch(ov_menu_win)) != KEY_F(1))
	{
		switch(c)
	    {	
			case KEY_DOWN:
				menu_driver(ov_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(ov_menu, REQ_UP_ITEM);
				break;
			case KEY_LEFT:
				menu_driver(ov_menu, REQ_LEFT_ITEM);
				break;
			case KEY_RIGHT:
				menu_driver(ov_menu, REQ_RIGHT_ITEM);
				break;
			case KEY_NPAGE:
				menu_driver(ov_menu, REQ_SCR_DPAGE);
				break;
			case KEY_PPAGE:
				menu_driver(ov_menu, REQ_SCR_UPAGE);
				break;
			case 10: /*Enter*/
			{	ITEM *cur;
				void (*p)(int);
		    	cur = current_item(ov_menu);
				p = item_userptr(cur);
				p(item_index(cur));
                refreshMenu(ov_menu_win);
				pos_menu_cursor(ov_menu);
				break;
			}
            break;
		}
        wrefresh(ov_menu_win);
	}	

	/* Unpost and free all the memory taken up */
        unpost_menu(ov_menu);
        free_menu(ov_menu);
        for(i = 0; i < n_choices; ++i)
                free_item(ov_items[i]);
	endwin();
	return (0);
}

void printTitle(WINDOW *win, int starty, int startx, int width, char *string, chtype color)
{	int length, x, y;
	float temp;

	if(win == NULL)
		win = stdscr;
	getyx(win, y, x);
	if(startx != 0)
		x = startx;
	if(starty != 0)
		y = starty;
	if(width == 0)
		width = 80;

	length = strlen(string);
	temp = (width - length)/ 2;
	x = startx + (int)temp;
	wattron(win, color);
	mvwprintw(win, y, x, "%s", string);
	wattroff(win, color);
	refresh();
}

void refreshMenu(WINDOW* menu_win){

	/* Print a border around the main window and print a title */
    box(menu_win, 0, 0);
	
	printTitle(menu_win, 1, 0, MENU_W, "Welcome to OpenVario", COLOR_PAIR(1));
	mvwaddch(menu_win, 2, 0, ACS_LTEE);
	mvwhline(menu_win, 2, 1, ACS_HLINE, MENU_W-2);
	mvwaddch(menu_win, 2, MENU_W-1, ACS_RTEE);
	
	attron(COLOR_PAIR(2));
	mvprintw(LINES - 3, 0, "IP Address: %s                       ",lan_ip);
	mvprintw(LINES - 2, 0, "                                                ");
	mvprintw(LINES - 1, 0, "Use Arrow Keys to navigate                      ");
	attroff(COLOR_PAIR(2));
	refresh();

}

void selectItem(int index)
{
	switch(index){
          case 0:
		system(xcsoar_cmd);
		break;
	  case 1:
		system(sensorcal_cmd);
		break;
	  case 2:
		endwin();
		if(fork() == 0)
			system(update_maps_usb_cmd);
		wait((int*) 0);
		raw();
		break;
	  case 3:
		endwin();
                if(fork() == 0)
                        system(update_system_usb_cmd);
                wait((int*) 0);
                raw();
                break;
	  case 4:
		endwin();
                if(fork() == 0)
                        system(download_igc_files_cmd);
                wait((int*) 0);
                raw();
                break;

	  case 5:
            	clear();
		endwin();
            	system("bash");
		break;
          case 6:
            	system("sudo startx &> /dev/null");
		break;
          case 7:
            	system("shutdown -P now");
		break;
 
        }
        clear();
	refresh();
}	

