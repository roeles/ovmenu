

all: build_ovmenu build_caltool

build_ovmenu :
	$(MAKE) -C ovmenu
	
build_caltool :
	$(MAKE) -C caltool

clean : 
	$(MAKE) -C ovmenu clean
	$(MAKE) -C caltool clean
